from flask import Flask
import utils
from flask import request
import flask 
app = Flask(__name__)

@app.route('/')
def default():
    return flask.render_template("index.html")

@app.route('/api/v1.0/login', methods=['GET'])
def get_tasks():
	key = request.args.get('key')
	json = utils.validate_key(key)
	if json == None:
		return ('None')
	else:
		return (json)

@app.route('/api/v1.0/save', methods=['POST'])
def save():
	key = request.args.get('key')
	new_data = request.get_json()
	utils.save_to_db(new_data,key)
	return "Saved fine!"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)